#include <stdint.h>

#define NUM_DECIMALS 14
#define NUM_WHOLES (31 - NUM-DECIMALS)
#define DECIMAL_MULT (1 << NUM_DECIMALS)

inline int CONVERT_TO_FIXED(int X);
inline int CONVERT_FROM_FIXED(int X);
inline int ROUND_NEG(int X);
inline int ROUND_POS(int X);
inline int CONVERT_FROM_FIXED_AND_ROUND(int X);
inline int MULTIPLY(int X, int Y);
inline int DIVIDE(int X, int Y);

inline int CONVERT_TO_FIXED(int X) {
  return X * DECIMAL_MULT;
}

inline int CONVERT_FROM_FIXED(int X) {
  return X / DECIMAL_MULT;
}

inline int ROUND_NEG(int X) {
  return (X - DECIMAL_MULT / 2) / DECIMAL_MULT;
}

inline int ROUND_POS(int X) {
  return (X + DECIMAL_MULT / 2) / DECIMAL_MULT;
}

inline int CONVERT_FROM_FIXED_AND_ROUND(int X) {
  return (X < 0) ? ROUND_NEG(X) : ROUND_POS(X);
}

inline int MULTIPLY(int X, int Y) {
  return ((int64_t) X) * Y / DECIMAL_MULT;
}

inline int DIVIDE(int X, int Y) {
  return ((int64_t) X) * DECIMAL_MULT / Y;
}
