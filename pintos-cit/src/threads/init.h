#ifndef THREADS_INIT_H
#define THREADS_INIT_H

#include <debug.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* Page directory with kernel mappings only. */
extern uint32_t *init_page_dir;

#ifdef VM
struct hash * frame_table;
struct list * frame_list;

struct bitmap * swap_list;
/*struct hash * swap_hash;
struct list * swap_list;*/
#endif

#ifdef FILESYS
void * file_cache;
struct hash * file_cache_map;
struct bitmap * free_cache_list;
struct list * used_cache_list;
#endif

#endif /* threads/init.h */

