#include "filesys/filesys.h"
#include <debug.h>
#include <stdio.h>
#include <string.h>
#include "filesys/cache.h"
#include "filesys/file.h"
#include "filesys/free-map.h"
#include "filesys/inode.h"
#include "filesys/directory.h"
#include "threads/malloc.h"
#include "threads/thread.h"

/*! Partition that contains the file system. */
struct block *fs_device;

static void do_format(void);

/*! Initializes the file system module.
    If FORMAT is true, reformats the file system. */
void filesys_init(bool format) {
    fs_device = block_get_role(BLOCK_FILESYS);
    if (fs_device == NULL)
        PANIC("No file system device found, can't initialize file system.");

    inode_init();
    free_map_init();

    if (format) 
        do_format();

    free_map_open();
}

/*! Shuts down the file system module, writing any unwritten data to disk. */
void filesys_done(void) {
    cache_flush();
    free_map_close();
}

/*! Creates a file named NAME with the given INITIAL_SIZE.  Returns true if
    successful, false otherwise.  Fails if a file named NAME already exists,
    or if internal memory allocation fails. */
bool filesys_create(const char *name, off_t initial_size, bool is_dir) {
    block_sector_t inode_sector = 0;
    size_t length = strlen(name);
    char *name_cpy = calloc(length+1, sizeof (char));
    strlcpy(name_cpy, name, length+1);
    struct dir *dir = dir_mostly_open_path(name_cpy);
    bool success = ((dir != NULL) &&
                    (!inode_is_removed(dir_get_inode(dir))) &&
                    (free_map_allocate(1, &inode_sector)) &&
                    (is_dir ? dir_create(inode_sector, 0,
                                         inode_get_inumber(dir_get_inode(dir)))
                            : inode_create(inode_sector, initial_size)) &&
                    (dir_add(dir, name_cpy, inode_sector, is_dir)));
    if (!success && inode_sector != 0) 
        free_map_release(inode_sector, 1);

    if (dir != thread_current()->cwd)
        dir_close(dir);

    free(name_cpy);
    return success;
}

/*! Opens the file with the given NAME.  Returns the new file if successful
    or a null pointer otherwise.  Fails if no file named NAME exists,
    or if an internal memory allocation fails. */
void * filesys_open(const char *name) {
    size_t length = strlen(name);
    char *name_cpy = calloc(length+1, sizeof (char));
    strlcpy(name_cpy, name, length+1);
    struct dir *dir = dir_mostly_open_path(name_cpy);
    struct inode *inode = NULL;

    void *res = NULL;
    if (dir != NULL && !inode_is_removed(dir_get_inode(dir))) {
        if (*name_cpy == '\0')
            res = dir;
        else if (dir_lookup(dir, name_cpy, &inode))
            res = dir_open(inode);
        else
            res = file_open(inode);
    }

    if (dir != thread_current()->cwd && dir != res)
        dir_close(dir);

    free(name_cpy);
    return res;
}

/*! Deletes the file named NAME.  Returns true if successful, false on failure.
    Fails if no file named NAME exists, or if an internal memory allocation
    fails. */
bool filesys_remove(const char *name) {
    size_t length = strlen(name);
    char *name_cpy = calloc(length+1, sizeof (char));
    strlcpy(name_cpy, name, length+1);
    struct dir *dir = dir_mostly_open_path(name_cpy);
    bool success = dir != NULL && dir_remove(dir, name_cpy);

    if (dir != thread_current()->cwd)
        dir_close(dir);

    free(name_cpy);
    return success;
}

bool filesys_change_cwd(const char *name) {
  struct thread * t = thread_current();
  size_t length = strlen(name);
  char *name_cpy = calloc(length+1, sizeof (char));
  strlcpy(name_cpy, name, length+1);
  struct dir * cwd = dir_mostly_open_path(name_cpy);
  struct inode *inode = NULL;
  
  if (cwd == NULL) {
    free(name_cpy);
    return false;
  }

  if (*name_cpy != '\0') {
    if (dir_lookup(cwd, name_cpy, &inode)) {
      dir_close(cwd);
      cwd = dir_open(inode);
    }
    else {
      dir_close(cwd);
      free(name_cpy);
      return false;
    }
  }

  if (cwd != thread_current()->cwd) {
      dir_close(t->cwd);
      t->cwd = cwd;
  }
  
  free(name_cpy);
  return true;
}

/*! Formats the file system. */
static void do_format(void) {
    printf("Formatting file system...");
    free_map_create();
    /* Make the root dir its own parent */
    if (!dir_create(ROOT_DIR_SECTOR, 16, ROOT_DIR_SECTOR))
        PANIC("root directory creation failed");
    free_map_close();
    printf("done.\n");
}

