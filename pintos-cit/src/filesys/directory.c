#include "filesys/directory.h"
#include <stdio.h>
#include <string.h>
#include <list.h>
#include "filesys/filesys.h"
#include "filesys/inode.h"
#include "threads/malloc.h"
#include "threads/thread.h"

/*! A directory. */
struct dir {
    bool is_dir;                        /*!< It is. */
    struct inode *inode;                /*!< Backing store. */
    off_t pos;                          /*!< Current position. */
};

/*! A single directory entry. */
struct dir_entry {
    block_sector_t inode_sector;        /*!< Sector number of header. */
    char name[NAME_MAX + 1];            /*!< Null terminated file name. */
    bool in_use;                        /*!< In use or free? */
    bool is_dir;
};

/*! Creates a directory with space for ENTRY_CNT entries in the
    given SECTOR.  Returns true if successful, false on failure. */
bool dir_create(block_sector_t sector, size_t entry_cnt, block_sector_t parent) {
  struct dir * new_dir;
  bool success;

  /* Add two for the . and .. directory */
  if (!inode_create(sector, (entry_cnt + 2) * sizeof(struct dir_entry))) {
    return false;
  }

  new_dir = dir_open(inode_open(sector));
  success = dir_add(new_dir, "..", parent, true) &&
            dir_add(new_dir, ".", sector, true);
  dir_close(new_dir);

  return success;
}

/*! Opens and returns the directory for the given INODE, of which
    it takes ownership.  Returns a null pointer on failure. */
struct dir * dir_open(struct inode *inode) {
    struct dir *dir = calloc(1, sizeof(*dir));
    if (inode != NULL && dir != NULL) {
        dir->is_dir = true;
        dir->inode = inode;
        dir->pos = 0;
        return dir;
    }
    else {
        inode_close(inode);
        free(dir);
        return NULL; 
    }
}

struct dir * dir_open_parent(const struct dir * directory) {
  struct inode * parent_inode;

  dir_lookup(directory, "..", &parent_inode);
  if (!parent_inode) {
    return NULL;
  }

  return dir_open(parent_inode);
}

struct dir * dir_open_path(const char * _path) {
  char * token, * save_ptr;
  struct dir * next_dir;
  struct thread * t = thread_current();
  struct dir * curr_dir = t->cwd;
  bool free_last = false;
  size_t length = strlen(_path);
  char * path = calloc(length + 1, sizeof (char));
  struct inode * curr_inode;

  strlcpy(path, _path, length);

  if (path[length - 1] != '/') {
    length++;
    strlcat(path, "/", length);
  }

  if (path[0] == '/') {
    curr_dir = dir_open_root();
    free_last = true;
  }

  for (token = strtok_r(path, "/", &save_ptr); token != NULL;
       token = strtok_r(NULL, "/", &save_ptr)) {
    dir_lookup(curr_dir, token, &curr_inode);
    if (!curr_inode) {
      if (free_last) {
        dir_close(curr_dir);
      }
      free (path);
      return NULL;
    }

    next_dir = dir_open(curr_inode);
    if (free_last) {
      dir_close(curr_dir);
    }

    curr_dir = next_dir;
    free_last = true;
  }

  free(path);
  return curr_dir;
}

/*! Open the directory containing the file at _PATH, which may be relative
    or absolute.  Removes the directory opened from _PATH, leaving only the
    filename. */
struct dir * dir_mostly_open_path(char * _path) {
  if (!_path)
    return NULL;

  if (*_path == '\0')
    return NULL;

  if (!strcmp(_path, "/")) {
    *_path = '\0';
    return dir_open_root();
  }

  char * token, * save_ptr;
  char *prev_token = NULL;
  struct dir * prev_dir = NULL;
  struct thread * t = thread_current();
  struct dir * curr_dir = t->cwd;
  size_t length = strlen(_path);
  char * path = calloc(length + 2, sizeof (char));
  struct inode * curr_inode;

  strlcpy(path, _path, length+1);

  if (curr_dir == NULL || path[0] == '/') {
    curr_dir = dir_open_root();
  }

  for (token = strtok_r(path, "/", &save_ptr); token != NULL;
       token = strtok_r(NULL, "/", &save_ptr)) { 

    if (prev_dir != NULL && prev_dir != t->cwd)
      dir_close(prev_dir);

    if (dir_lookup(curr_dir, token, &curr_inode)) {
      prev_dir = curr_dir;
      curr_dir = dir_open(curr_inode);
    }

    else if (strtok_r(NULL, "/", &save_ptr)) {
      if (curr_dir != NULL && curr_dir != t->cwd)
        dir_close(curr_dir);
      free (path);
      return NULL;
    }

    else {
      prev_dir = curr_dir;
    }

    prev_token = token;
  }
  if (prev_dir != curr_dir && curr_dir != NULL && curr_dir != t->cwd)
    dir_close(curr_dir);

  if (prev_token) {
    strlcpy(_path, prev_token, length+1);
  }
  free(path);
  return prev_dir;
}

/*! Opens the root directory and returns a directory for it.
    Return true if successful, false on failure. */
struct dir * dir_open_root(void) {
    return dir_open(inode_open(ROOT_DIR_SECTOR));
}

/* Opens and returns a new directory for the same inode as DIR.
   Returns a null pointer on failure. */
struct dir * dir_reopen(struct dir *dir) {
    return dir_open(inode_reopen(dir->inode));
}

/*! Destroys DIR and frees associated resources. */
void dir_close(struct dir *dir) {
    if (dir != NULL) {
        inode_close(dir->inode);
        free(dir);
    }
}

/*! Returns the inode encapsulated by DIR. */
struct inode * dir_get_inode(struct dir *dir) {
    return dir->inode;
}

/*! Searches DIR for a file with the given NAME.
    If successful, returns true, sets *EP to the directory entry
    if EP is non-null, and sets *OFSP to the byte offset of the
    directory entry if OFSP is non-null.
    otherwise, returns false and ignores EP and OFSP. */
static bool lookup(const struct dir *dir, const char *name,
                   struct dir_entry *ep, off_t *ofsp) {
    struct dir_entry e;
    size_t ofs;

    ASSERT(dir != NULL);
    ASSERT(name != NULL);

    for (ofs = 0; inode_read_at(dir->inode, &e, sizeof(e), ofs) == sizeof(e);
         ofs += sizeof(e)) {
        if (e.in_use && !strcmp(name, e.name)) {
            if (ep != NULL)
                *ep = e;
            if (ofsp != NULL)
                *ofsp = ofs;
            return true;
        }
    }
    return false;
}

/*! Searches DIR for a file with the given NAME and returns true if one exists
    and is a directory, and returns false otherwise.  On success, sets *INODE to
    an inode for the file, otherwise to a null pointer.  The caller must close *INODE. */
bool dir_lookup(const struct dir *dir, const char *name, struct inode **inode) {
    struct dir_entry e;

    ASSERT(dir != NULL);
    ASSERT(name != NULL);

    if (lookup(dir, name, &e, NULL))
        *inode = inode_open(e.inode_sector);
    else
        *inode = NULL;

    return (*inode != NULL) && e.is_dir;
}

/*! Adds a file named NAME to DIR, which must not already contain a file by
    that name.  The file's inode is in sector INODE_SECTOR.
    Returns true if successful, false on failure.
    Fails if NAME is invalid (i.e. too long) or a disk or memory
    error occurs. */
bool dir_add(struct dir *dir, const char *name, block_sector_t inode_sector, bool is_dir) {
    struct dir_entry e;
    off_t ofs;
    bool success = false;

    ASSERT(dir != NULL);
    ASSERT(name != NULL);

    /* Check that DIR is not removed. */
    if (inode_is_removed(dir_get_inode(dir)))
        return false;

    /* Check NAME for validity. */
    if (*name == '\0' || strlen(name) > NAME_MAX)
        return false;

    /* Check that NAME is not in use. */
    if (lookup(dir, name, NULL, NULL))
        goto done;

    /* Set OFS to offset of free slot.
       If there are no free slots, then it will be set to the
       current end-of-file.
     
       inode_read_at() will only return a short read at end of file.
       Otherwise, we'd need to verify that we didn't get a short
       read due to something intermittent such as low memory. */
    for (ofs = 0; inode_read_at(dir->inode, &e, sizeof(e), ofs) == sizeof(e);
         ofs += sizeof(e)) {
        if (!e.in_use)
            break;
    }

    /* Write slot. */
    e.in_use = true;
    e.is_dir = is_dir;
    strlcpy(e.name, name, sizeof e.name);
    e.inode_sector = inode_sector;
    success = inode_write_at(dir->inode, &e, sizeof(e), ofs) == sizeof(e);

done:
    return success;
}

/*! Removes any entry for NAME in DIR.  Returns true if successful, false on
    failure, which occurs if there is no file with the given NAME or if the
    given NAME refers to a non-empty subdirectory. */
bool dir_remove(struct dir *dir, const char *name) {
    struct dir_entry e;
    struct inode *inode = NULL;
    bool success = false;
    off_t ofs;

    ASSERT(dir != NULL);
    ASSERT(name != NULL);

    /* Find directory entry. */
    if (!lookup(dir, name, &e, &ofs))
        goto done;

    /* Open inode. */
    inode = inode_open(e.inode_sector);
    if (inode == NULL)
        goto done;

    /* Check for a non-empty directory. */
    if (e.is_dir) {
        struct dir *target = dir_open(inode);
        char name[NAME_MAX + 1];
        while (dir_readdir(target, name))
            if (strcmp(".", name) && strcmp("..", name)) {
                dir_close(target);
                goto done;
            }
        dir_close(target);
    }

    /* Erase directory entry. */
    e.in_use = false;
    if (inode_write_at(dir->inode, &e, sizeof(e), ofs) != sizeof(e))
        goto done;

    /* Remove inode. */
    inode_remove(inode);
    success = true;

done:
    inode_close(inode);
    return success;
}

/*! Reads the next directory entry in DIR and stores the name in NAME.  Returns
    true if successful, false if the directory contains no more entries. */
bool dir_readdir(struct dir *dir, char name[NAME_MAX + 1]) {
    struct dir_entry e;

    while (inode_read_at(dir->inode, &e, sizeof(e), dir->pos) == sizeof(e)) {
        dir->pos += sizeof(e);
        if (e.in_use) {
            strlcpy(name, e.name, NAME_MAX + 1);
            return true;
        } 
    }
    return false;
}

