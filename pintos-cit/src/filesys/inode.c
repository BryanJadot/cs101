#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include "filesys/cache.h"
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "threads/malloc.h"
#include "threads/synch.h"

/*! Identifies an inode. */
#define INODE_MAGIC 0x494e4f44

#define SECTORS_PER_TABLE (BLOCK_SECTOR_SIZE / sizeof (block_sector_t))

/*! On-disk inode.
    Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk {
    block_sector_t start;               /*!< First data sector. */
    off_t length;                       /*!< File size in bytes. */
    size_t last_st_idx;                 /*!< Index of last allocated sector table. */
    size_t last_sector_idx;             /*!< Index of last allocated sector. */
    block_sector_t secdir;              /*!< Sectors containing file sector directory. */
    unsigned magic;                     /*!< Magic number. */
    block_sector_t unused[122];
};

/*! Reserves and indexes SECTORS additional sectors for a file.
    Returns TRUE if all requested sectors were successfully allocated,
    returns FALSE otherwise. */
static bool inode_disk_add_sectors(struct inode_disk *inode, size_t sectors) {
    if (sectors == 0)
        return true;

    size_t sector_idx = inode->last_sector_idx;
    size_t st_idx = inode->last_st_idx;
    block_sector_t *st = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    block_sector_t *sd = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    memset(sd, 0, BLOCK_SECTOR_SIZE);
    memset(st, 0, BLOCK_SECTOR_SIZE);
    bool success = true;

    /* Set up the sector directory and sector table. */
    if (inode->start == (block_sector_t) -1) {
        if (!free_map_allocate(1, &inode->secdir)) {
            success = false;
            sectors = 0;
        }
        else if (!free_map_allocate(1, &sd[st_idx])) {
            free_map_release(inode->secdir, 1);
            success = false;
            sectors = 0;
        }
        else if (!(free_map_allocate(1, &inode->start))) {
            free_map_release(sd[st_idx], 1);
            free_map_release(inode->secdir, 1);
            success = false;
            sectors = 0;
        }
        else {
            st[sector_idx] = inode->start;
            sectors--;
        }
    }
    else {
        cache_read(fs_device, inode->secdir, sd);
        cache_read(fs_device, sd[st_idx], st);
    }
    
    /* Allocate the sectors. */
    while (sectors > 0) {
        sector_idx++;
        sector_idx = sector_idx % SECTORS_PER_TABLE;

        /* Allocate new sector table if necessary. */
        if (sector_idx == 0) {
            cache_write(fs_device, sd[st_idx], st);
            st_idx++;
            ASSERT(st_idx < 128);
            if (!free_map_allocate(1, &sd[st_idx])) {
                cache_write(fs_device, inode->secdir, sd);
                success = false;
                break;
            }
            inode->last_st_idx = st_idx;
            memset(st, 0, BLOCK_SECTOR_SIZE);
        }

        /* Allocate new sector. */
        if (!(free_map_allocate(1, &st[sector_idx]))) {
            cache_write(fs_device, inode->secdir, sd);
            cache_write(fs_device, sd[st_idx], st);
            success = false;
            break;
        }
        inode->last_sector_idx = sector_idx;
        static char zeros[BLOCK_SECTOR_SIZE];
        cache_write(fs_device, st[sector_idx], zeros);
        sectors--;
    }

    if (success) {
        cache_write(fs_device, inode->secdir, sd);
        cache_write(fs_device, sd[st_idx], st);
    }

    free(st);
    free(sd);
    
    return success;
}
        
/*! Returns the SECTOR_IDX'th sector in the file. */
static block_sector_t inode_disk_get_sector(const struct inode_disk *inode,
                                            size_t sector_idx) {
    block_sector_t *sd = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    block_sector_t *st = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    
    cache_read(fs_device, inode->secdir, sd);
    cache_read(fs_device, sd[sector_idx / SECTORS_PER_TABLE], st);

    block_sector_t sector = st[sector_idx % SECTORS_PER_TABLE];

    free(sd);
    free(st);

    return sector;
}

/*! Frees all the sectors held by INODE. */
static void inode_disk_clear_sectors(struct inode_disk *inode) {
    block_sector_t *sd = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    block_sector_t *st = calloc(SECTORS_PER_TABLE, sizeof (block_sector_t));
    size_t st_idx;
    size_t sector_idx;
    
    if (inode->start == (block_sector_t) -1)
        return;

    cache_read(fs_device, inode->secdir, sd);

    for (st_idx = 0; st_idx < inode->last_st_idx; st_idx++) {
        cache_read(fs_device, sd[st_idx], st);
        for (sector_idx = 0; sector_idx < SECTORS_PER_TABLE; sector_idx++) {
            free_map_release(st[sector_idx], 1);
        }
    }
    cache_read(fs_device, sd[st_idx], st);
    for (sector_idx = 0; sector_idx <= inode->last_sector_idx; sector_idx++) {
        free_map_release(st[sector_idx], 1);
    }

    free_map_release(inode->secdir, 1);

    inode->secdir = 0;
    inode->last_sector_idx = 0;
    inode->last_st_idx = 0;
    inode->start = -1;

    free(st);
    free(sd);

    return;
}

/*! Returns the number of sectors to allocate for an inode SIZE
    bytes long. */
static inline size_t bytes_to_sectors(off_t size) {
    return DIV_ROUND_UP(size, BLOCK_SECTOR_SIZE);
}

/*! In-memory inode. */
struct inode {
    struct list_elem elem;              /*!< Element in inode list. */
    block_sector_t sector;              /*!< Sector number of disk location. */
    int open_cnt;                       /*!< Number of openers. */
    bool removed;                       /*!< True if deleted, false otherwise. */
    int deny_write_cnt;                 /*!< 0: writes ok, >0: deny writes. */
    struct lock lock;                   /*!< Lock to synchronize file extension. */
};

static bool inode_add_sectors(struct inode *inode, size_t sectors) {
    struct inode_disk *disk_inode = calloc(1, sizeof(struct inode_disk));
    cache_read(fs_device, inode->sector, disk_inode);
    bool success = inode_disk_add_sectors(disk_inode, sectors);
    cache_write(fs_device, inode->sector, disk_inode);
    free(disk_inode);
    return success;
}

static block_sector_t inode_get_sector(const struct inode *inode, size_t sector_idx) {
    struct inode_disk *disk_inode = calloc(1, sizeof(struct inode_disk));
    cache_read(fs_device, inode->sector, disk_inode);
    block_sector_t sector = inode_disk_get_sector(disk_inode, sector_idx);
    free(disk_inode);
    return sector;
}

static void inode_clear_sectors(struct inode *inode) {
    struct inode_disk *disk_inode = calloc(1, sizeof(struct inode_disk));
    cache_read(fs_device, inode->sector, disk_inode);
    inode_disk_clear_sectors(disk_inode);
    cache_write(fs_device, inode->sector, disk_inode);
    free(disk_inode);
}

/*! Returns the block device sector that contains byte offset POS
    within INODE.
    Returns -1 if INODE does not contain data for a byte at offset
    POS. */
static block_sector_t byte_to_sector(const struct inode *inode, off_t pos) {
    ASSERT(inode != NULL);
    return inode_get_sector(inode, pos / BLOCK_SECTOR_SIZE);
}

/*! List of open inodes, so that opening a single inode twice
    returns the same `struct inode'. */
static struct list open_inodes;

/*! Initializes the inode module. */
void inode_init(void) {
    list_init(&open_inodes);
}

/*! Initializes an inode with LENGTH bytes of data and
    writes the new inode to sector SECTOR on the file system
    device.
    Returns true if successful.
    Returns false if memory or disk allocation fails. */
bool inode_create(block_sector_t sector, off_t length) {
    struct inode_disk *disk_inode = NULL;
    bool success = false;

    ASSERT(length >= 0);

    /* If this assertion fails, the inode structure is not exactly
       one sector in size, and you should fix that. */
    ASSERT(sizeof *disk_inode == BLOCK_SECTOR_SIZE);

    disk_inode = calloc(1, sizeof *disk_inode);
    if (disk_inode != NULL) {
        size_t sectors = bytes_to_sectors(length);
        disk_inode->start = -1;
        disk_inode->length = length;
        disk_inode->magic = INODE_MAGIC;
        if (inode_disk_add_sectors(disk_inode, sectors)) {
            cache_write(fs_device, sector, disk_inode);
            success = true; 
        }
        else {
            inode_disk_clear_sectors(disk_inode);
        }
        free(disk_inode);
    }
    return success;
}

/*! Reads an inode from SECTOR
    and returns a `struct inode' that contains it.
    Returns a null pointer if memory allocation fails. */
struct inode * inode_open(block_sector_t sector) {
    struct list_elem *e;
    struct inode *inode;

    /* Check whether this inode is already open. */
    for (e = list_begin(&open_inodes); e != list_end(&open_inodes);
         e = list_next(e)) {
        inode = list_entry(e, struct inode, elem);
        if (inode->sector == sector) {
            inode_reopen(inode);
            return inode; 
        }
    }

    /* Allocate memory. */
    inode = malloc(sizeof *inode);
    if (inode == NULL)
        return NULL;

    /* Initialize. */
    list_push_front(&open_inodes, &inode->elem);
    inode->sector = sector;
    inode->open_cnt = 1;
    inode->deny_write_cnt = 0;
    inode->removed = false;
    lock_init(&inode->lock);
    return inode;
}

/*! Reopens and returns INODE. */
struct inode * inode_reopen(struct inode *inode) {
    if (inode != NULL)
        inode->open_cnt++;
    return inode;
}

/*! Returns INODE's inode number. */
block_sector_t inode_get_inumber(const struct inode *inode) {
    return inode->sector;
}

/*! Closes INODE and writes it to disk.
    If this was the last reference to INODE, frees its memory.
    If INODE was also a removed inode, frees its blocks. */
void inode_close(struct inode *inode) {
    /* Ignore null pointer. */
    if (inode == NULL)
        return;

    /* Release resources if this was the last opener. */
    if (--inode->open_cnt == 0) {
        /* Remove from inode list and release lock. */
        list_remove(&inode->elem);
 
        /* Deallocate blocks if removed. */
        if (inode->removed) {
            inode_clear_sectors(inode);
            free_map_release(inode->sector, 1);
        }

        free(inode); 
    }
}

/*! Marks INODE to be deleted when it is closed by the last caller who
    has it open. */
void inode_remove(struct inode *inode) {
    ASSERT(inode != NULL);
    inode->removed = true;
}

/*! Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t inode_read_at(struct inode *inode, void *buffer_, off_t size, off_t offset) {
    uint8_t *buffer = buffer_;
    off_t bytes_read = 0;
    uint8_t *bounce = NULL;

    while (size > 0) {
        /* Disk sector to read, starting byte offset within sector. */
        block_sector_t sector_idx = byte_to_sector (inode, offset);
        int sector_ofs = offset % BLOCK_SECTOR_SIZE;

        /* Bytes left in inode, bytes left in sector, lesser of the two. */
        off_t inode_left = inode_length(inode) - offset;
        int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
        int min_left = inode_left < sector_left ? inode_left : sector_left;

        /* Number of bytes to actually copy out of this sector. */
        int chunk_size = size < min_left ? size : min_left;
        if (chunk_size <= 0)
            break;

        if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE) {
            /* Read full sector directly into caller's buffer. */
            cache_read(fs_device, sector_idx, buffer + bytes_read);
        }
        else {
            /* Read sector into bounce buffer, then partially copy
               into caller's buffer. */
            if (bounce == NULL) {
                bounce = malloc(BLOCK_SECTOR_SIZE);
                if (bounce == NULL)
                    break;
            }
            cache_read(fs_device, sector_idx, bounce);
            memcpy(buffer + bytes_read, bounce + sector_ofs, chunk_size);
        }
      
        /* Advance. */
        size -= chunk_size;
        offset += chunk_size;
        bytes_read += chunk_size;
    }
    free(bounce);

    return bytes_read;
}

static void inode_set_length(const struct inode *inode, off_t length) {
    struct inode_disk *disk_inode = calloc(1, sizeof(struct inode_disk));
    cache_read(fs_device, inode->sector, disk_inode);
    disk_inode->length = length;
    cache_write(fs_device, inode->sector, disk_inode);
    free(disk_inode);
}

/*! Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
    Returns the number of bytes actually written, which may be
    less than SIZE if an error occurs. */
off_t inode_write_at(struct inode *inode, const void *buffer_, off_t size, off_t offset) {
    const uint8_t *buffer = buffer_;
    off_t bytes_written = 0;
    uint8_t *bounce = NULL;

    if (inode->deny_write_cnt)
        return 0;

    off_t length = inode_length(inode);
    off_t old_length = length;

    while (size > 0) {
        while (size > 0) {
            /* Sector to write, starting byte offset within sector. */
            block_sector_t sector_idx = byte_to_sector(inode, offset);
            int sector_ofs = offset % BLOCK_SECTOR_SIZE;

            /* Bytes left in inode, bytes left in sector, lesser of the two. */
            off_t inode_left = length - offset;
            int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
            int min_left = inode_left < sector_left ? inode_left : sector_left;

            /* Number of bytes to actually write into this sector. */
            int chunk_size = size < min_left ? size : min_left;
            if (chunk_size <= 0)
                break;

            if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE) {
                /* Write full sector directly to disk. */
                cache_write(fs_device, sector_idx, buffer + bytes_written);
            }
            else {
                /* We need a bounce buffer. */
                if (bounce == NULL) {
                    bounce = malloc(BLOCK_SECTOR_SIZE);
                    if (bounce == NULL)
                        break;
                }

                /* If the sector contains data before or after the chunk
                   we're writing, then we need to read in the sector
                   first.  Otherwise we start with a sector of all zeros. */
    
                if (sector_ofs > 0 || chunk_size < sector_left) 
                    cache_read(fs_device, sector_idx, bounce);
                else
                    memset (bounce, 0, BLOCK_SECTOR_SIZE);
    
                memcpy(bounce + sector_ofs, buffer + bytes_written, chunk_size);
                cache_write(fs_device, sector_idx, bounce);
            }
    
            /* Advance. */
            size -= chunk_size;
            offset += chunk_size;
            bytes_written += chunk_size;
        }
        free(bounce);

        /* Extend the file, if necessary. */
        if (size > 0) {
            lock_acquire(&inode->lock);
            inode_add_sectors(inode, bytes_to_sectors(offset + size) - 
                                     bytes_to_sectors(length));
            length = offset + size;
        }
    }
    if (old_length < length) {
            inode_set_length(inode, length);
            lock_release(&inode->lock);
    }

    return bytes_written;
}

/*! Disables writes to INODE.
    May be called at most once per inode opener. */
void inode_deny_write (struct inode *inode) {
    inode->deny_write_cnt++;
    ASSERT(inode->deny_write_cnt <= inode->open_cnt);
}

/*! Re-enables writes to INODE.
    Must be called once by each inode opener who has called
    inode_deny_write() on the inode, before closing the inode. */
void inode_allow_write (struct inode *inode) {
    ASSERT(inode->deny_write_cnt > 0);
    ASSERT(inode->deny_write_cnt <= inode->open_cnt);
    inode->deny_write_cnt--;
}

/*! Returns the length, in bytes, of INODE's data. */
off_t inode_length(const struct inode *inode) {
    struct inode_disk *disk_inode = calloc(1, sizeof(struct inode_disk));
    cache_read(fs_device, inode->sector, disk_inode);
    off_t length = disk_inode->length;
    free(disk_inode);
    return length;
}

/*! Returns TRUE if INODE has been removed. */
bool inode_is_removed(const struct inode *inode) {
    if (!inode)
        return false;
    return inode->removed;
}
