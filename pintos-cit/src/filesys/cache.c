#include "filesys/cache.h"

#include <hash.h>
#include <bitmap.h>
#include <list.h>
#include <string.h>

#include "devices/block.h"
#include "devices/timer.h"
#include "threads/init.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "userprog/process.h"

static unsigned cache_hash (const struct hash_elem *element,
                            void * aux UNUSED);
static bool cache_less (const struct hash_elem *a,
                        const struct hash_elem *b,
                        void *aux UNUSED);
static void * evict_file(void);
static void * find_free_slot(void);
static inline size_t slot_to_index(void * slot);
static void periodic_flush(void *);
static void read_ahead(void *);
static void add_cache_slot(struct block *, block_sector_t, void *);

struct lock write_lock;

void cache_init() {
  file_cache = malloc(CACHE_SIZE * BLOCK_SECTOR_SIZE);

  file_cache_map = malloc(sizeof (struct hash));
  hash_init(file_cache_map, cache_hash, cache_less, NULL);

  used_cache_list = malloc(sizeof (struct list));
  list_init(used_cache_list);

  free_cache_list = bitmap_create(CACHE_SIZE);
  bitmap_set_all(free_cache_list, true);

  lock_init(&write_lock);

  thread_create("Cache flush", PRI_DEFAULT, periodic_flush, NULL, thread_current()->cwd);
}

static void read_ahead(void * aux) {
  struct cache_elem elem;
  struct hash_elem * h_elem;
  void * free_slot;

  lock_acquire(&write_lock);
  struct read_ahead_struct * read_ahead = (struct read_ahead_struct *)aux;
  struct block * fs_dev = read_ahead->block;
  block_sector_t s_idx = read_ahead->sector;
  
  elem.block = fs_dev;
  elem.sector = s_idx;
  h_elem = hash_find(file_cache_map, &elem.elem);
  if (h_elem == NULL) {
    free_slot = find_free_slot();
    
    if (free_slot == NULL) {
      free_slot = evict_file();
    }
    
    add_cache_slot(fs_dev, s_idx, free_slot);
    block_read(fs_dev, s_idx, free_slot);
  }
  free(aux);
  lock_release(&write_lock);

  process_exit_with_status(0);
}

static void periodic_flush(void * aux UNUSED) {
  while (true) {
    timer_sleep(1000);
    cache_flush();
  }
}

static unsigned cache_hash (const struct hash_elem *element,
                            void * aux UNUSED) {
  ASSERT (element != NULL);

  struct cache_elem *elem = hash_entry(element, struct cache_elem, elem);

  return hash_bytes (&elem->block, sizeof (elem->block) + sizeof (elem->sector));
}

static bool cache_less (const struct hash_elem *a,
                        const struct hash_elem *b,
                        void *aux UNUSED) {
  ASSERT (a != NULL && b != NULL);

  struct cache_elem *cache_a = hash_entry(a, struct cache_elem, elem);
  struct cache_elem *cache_b = hash_entry(b, struct cache_elem, elem);

  if (cache_a->block != cache_b->block) {
    return cache_a->block < cache_b->block;
  }

  return cache_a->sector < cache_b->sector;

}

static void * evict_file() {
  struct list_elem * lst_ptr, * next_ptr;
  struct cache_elem * ce;
  uint32_t slot_idx;
  void * location;

  lst_ptr = list_begin(used_cache_list);
  while (lst_ptr != list_end(used_cache_list)) {
    ce = list_entry(lst_ptr, struct cache_elem, l_elem);
    
    if (ce->accessed) {
      ce->accessed = false;

      next_ptr = list_remove(lst_ptr);
      list_push_back(used_cache_list, lst_ptr);

      lst_ptr = next_ptr;
    } else {
      /* Write the buffer back to the disk */
      block_write(ce->block, ce->sector, ce->location);
      
      slot_idx = slot_to_index(ce->location);
      bitmap_set(free_cache_list, slot_idx, true);
      
      list_remove(lst_ptr);
      hash_delete(file_cache_map, &ce->elem);

      location = ce->location;

      free(ce);

      return location;
    }
  }

  PANIC ("Cannot find a place to cache the file");
}

void cache_flush() {
  struct list_elem * lst_ptr;
  struct cache_elem * ce;
  lock_acquire(&write_lock);
  for (lst_ptr = list_begin(used_cache_list);
       lst_ptr != list_end(used_cache_list);
       lst_ptr = list_next(lst_ptr)) {
    ce = list_entry(lst_ptr, struct cache_elem, l_elem);
    block_write(ce->block, ce->sector, ce->location);
  }
  lock_release(&write_lock);
}

static void * find_free_slot() {
  size_t idx = bitmap_scan(free_cache_list, 0, 1, true);
  if (idx == BITMAP_ERROR) {
    return NULL;
  } else {
    return file_cache + idx * BLOCK_SECTOR_SIZE;
  }
}

static void add_cache_slot(struct block * fs_dev, block_sector_t s_idx, void * location) {
  struct cache_elem * elem = malloc(sizeof (struct cache_elem));

  elem->block = fs_dev;
  elem->sector = s_idx;
  elem->location = location;
  elem->accessed = true;

  hash_insert(file_cache_map, &elem->elem);
  list_push_back(used_cache_list, &elem->l_elem);
  bitmap_set(free_cache_list, slot_to_index(location), false);
}

static inline size_t slot_to_index(void * slot) {
  return (size_t)(slot - file_cache) / BLOCK_SECTOR_SIZE;
}

void cache_write(struct block * fs_dev, block_sector_t s_idx, const void * buffer) {
  struct cache_elem elem;
  struct hash_elem * h_elem;
  void * free_slot;
  
  lock_acquire(&write_lock);
  
  elem.block = fs_dev;
  elem.sector = s_idx;
  h_elem = hash_find(file_cache_map, &elem.elem);
  if (h_elem == NULL) {
    free_slot = find_free_slot();
    
    if (free_slot == NULL) {
      free_slot = evict_file();
    }

    add_cache_slot(fs_dev, s_idx, free_slot);
    memcpy (free_slot, buffer, BLOCK_SECTOR_SIZE);
  } else {
    elem = *hash_entry(h_elem, struct cache_elem, elem);
    elem.accessed = true;
    memcpy (elem.location, buffer, BLOCK_SECTOR_SIZE);
  }
  lock_release(&write_lock);
}

void cache_read(struct block * fs_dev, block_sector_t s_idx, void * buffer) {
    struct cache_elem elem;
    struct hash_elem * h_elem;
    void * free_slot;

    lock_acquire(&write_lock);
    elem.block = fs_dev;
    elem.sector = s_idx;
    h_elem = hash_find(file_cache_map, &elem.elem);
    if (h_elem == NULL) {
      free_slot = find_free_slot();

      if (free_slot == NULL) {
        free_slot = evict_file();
      }

      add_cache_slot(fs_dev, s_idx, free_slot);
      block_read(fs_dev, s_idx, free_slot);
      
      memcpy (buffer, free_slot, BLOCK_SECTOR_SIZE);
/*
      if (s_idx < block_size(fs_dev) - 1) {
        struct read_ahead_struct * ras =
          malloc(sizeof(struct read_ahead_struct));

        ras->block = fs_dev;
        ras->sector = s_idx + 1;
        thread_create("Read ahead", PRI_DEFAULT, read_ahead, ras, thread_current()->cwd);
      }
*/
    } else {
      elem = *hash_entry(h_elem, struct cache_elem, elem);
      elem.accessed = true;
     
      memcpy (buffer, elem.location, BLOCK_SECTOR_SIZE);
    }
    lock_release(&write_lock);
}


