#ifndef FILESYS_CACHE_H
#define FILESYS_CACHE_H

#include "devices/block.h"
#include <list.h>
#include <hash.h>

#define CACHE_SIZE 64

/* Maps file and offset to location in cache */
struct cache_elem {
  struct block * block;
  block_sector_t sector;
  void * location;
  bool accessed;

  struct list_elem l_elem;
  struct hash_elem elem;
};

struct read_ahead_struct {
  struct block * block;
  block_sector_t sector;
};

void cache_init(void);
void cache_read(struct block *, block_sector_t, void *);
void cache_write(struct block *, block_sector_t, const void *);
void cache_flush(void);

#endif
