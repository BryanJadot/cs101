#include "devices/block.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/pagedir.h"
#include "userprog/syscall.h"
#include "vm/page.h"
#include "vm/swap.h"

static unsigned spt_hash_func(const struct hash_elem *, void *);
static bool spt_less_func(const struct hash_elem *,
                          const struct hash_elem *,
                          void *);
static void spt_unmap_elem(struct hash_elem *, void *);
static struct spt_elem *spt_get_entry(struct spt *, void *);


/* Hash function for elements in supplemental page table. */
static unsigned spt_hash_func(const struct hash_elem * elem, void * aux UNUSED) {
    void * page = hash_entry(elem, struct spt_elem, elem)->page;
    return hash_int(SPT_KEY((int)page));
}

/* Comparison function for elements in supplemental page table. */
static bool spt_less_func(const struct hash_elem *a,
                          const struct hash_elem *b,
                          void *aux UNUSED) {
  uint32_t page_a = (uint32_t)hash_entry(a, struct spt_elem, elem)->page;
  uint32_t  page_b = (uint32_t)hash_entry(b, struct spt_elem, elem)->page;
  return SPT_KEY(page_a) < SPT_KEY(page_b);
}

/* Hash action function to unmap an spt_elem. */
static void spt_unmap_elem(struct hash_elem *h_elem, void *aux UNUSED) {
    if (h_elem) {
        struct spt_elem *elem = hash_entry(h_elem, struct spt_elem, elem);
        uint32_t *pd = thread_current()->pagedir;
        void *page = (void *) SPT_KEY(elem->page);
        
        /* Write to file if elem is a dirty mmapped file page. */
        if (elem->flags & SPT_MMAP && pagedir_is_dirty(pd, page)) {
            acquire_file_lock();
            struct file *file = elem->origin;
            file_seek(file, elem->file_offset);
            file_write(file, page, elem->file_size);
            release_file_lock();
        }

        /* Clear the page table entry for elem. */
        pagedir_clear_page(pd, page);

        /* Free the memory held by elem. */
        free(elem);
    }
}

/* Returns the element of SPT corresponding with PAGE.
   Returns NULL if no such element exists. */
static struct spt_elem * spt_get_entry(struct spt *spt, void *page) {
    struct hash_elem *h_elem;
    struct spt_elem elem;
    elem.page = page;

    h_elem = hash_find(&spt->table, &elem.elem);
    if (h_elem)
        return hash_entry(h_elem, struct spt_elem, elem);
    return NULL;
}
    
/* Initialize SPT as an empty supplemental page table.
   Returns true on success, false on failure. */bool spt_init(struct spt *spt) {
    return hash_init(&spt->table, &spt_hash_func, &spt_less_func, NULL);
}

/* Frees all memory held by SPT. */
void spt_destroy(struct spt *spt) {
    hash_destroy(&spt->table, &spt_unmap_elem);
}

/* Sets the origin of the element of SPT corresponding with PAGE to position 
   OFFSET within FILE, creating a new element if one does not already exist. */
void spt_map_to_file(struct spt *spt, void *page, struct file *file,
                     off_t offset, size_t size, bool writable) {
    struct hash_elem *h_elem;
    struct spt_elem *elem = (struct spt_elem *) malloc(sizeof (struct spt_elem));
    elem->page = page;
    elem->flags = SPT_FILE;
    elem->file_offset = offset;
    elem->file_size = size;
    elem->origin = file;
    elem->mapid = -1;
    if (writable)
        elem->flags |= SPT_WRITABLE;

    h_elem = hash_replace(&spt->table, &elem->elem);
    if (h_elem)
        free(hash_entry(h_elem, struct spt_elem, elem));
}

/* Sets the origin of the element of SPT corresponding with PAGE to SWAP,
   creating a new element if one does not already exist. */
void spt_map_to_swap(struct spt *spt, void *page, void *swap, bool writable) {
    struct hash_elem *h_elem;
    struct spt_elem *elem = (struct spt_elem *) malloc(sizeof (struct spt_elem));
    elem->page = page;
    elem->flags = SPT_SWAP;
    elem->file_offset = 0;
    elem->origin = swap;
    elem->mapid = -1;
    if (writable)
        elem->flags |= SPT_WRITABLE;

    h_elem = hash_replace(&spt->table, &elem->elem);
    if (h_elem)
        free(hash_entry(h_elem, struct spt_elem, elem));
}

/* Sets the element of SPT corresponding with PAGE to have no external origin,
   creating a new element if one does not already exist. */
void spt_map_to_empty(struct spt *spt, void *page, bool writable) {
    struct hash_elem *h_elem;
    struct spt_elem *elem = (struct spt_elem *) malloc(sizeof (struct spt_elem));
    elem->page = page;
    elem->flags = 0;
    elem->file_offset = 0;
    elem->origin = NULL;
    elem->mapid = -1;
    if (writable)
        elem->flags |= SPT_WRITABLE;

    h_elem = hash_replace(&spt->table, &elem->elem);
    if (h_elem)
        free(hash_entry(h_elem, struct spt_elem, elem));
}

/* Memory maps FILE to location PAGE. */
void spt_mmap(struct spt *spt, void *page,
              mapid_t mapid, struct file *file,
              off_t offset, size_t size, bool writable) {
    struct hash_elem *h_elem;
    struct spt_elem *elem = (struct spt_elem *) malloc(sizeof (struct spt_elem));
    elem->page = page;
    elem->flags = SPT_FILE | SPT_MMAP;
    elem->file_offset = offset;
    elem->file_size = size;
    elem->origin = file;
    elem->mapid = mapid;
    if (writable)
        elem->flags |= SPT_WRITABLE;

    h_elem = hash_replace(&spt->table, &elem->elem);
    if (h_elem)
        free(hash_entry(h_elem, struct spt_elem, elem));
}   

/* Removes the element of SPT corresponding with PAGE, if one exists.
   Returns true if an element was removed, returns false otherwise. */
bool spt_unmap(struct spt *spt, void *page) {
    struct hash_elem *h_elem;
    struct spt_elem elem;
    elem.page = page;

    h_elem = hash_delete(&spt->table, &elem.elem);
    if (h_elem) {
        spt_unmap_elem(h_elem, NULL);
        return true;
    }
    return false;
}

/* Unmaps the entire file memory mapped to address ADDR,
   wirting dirty pages.  Returns the file unmapped without
   closing it. */
struct file *spt_munmap(struct spt *spt, mapid_t mapid, void *addr) {
    struct file *file = NULL;
    if (spt_get_mapid(spt, addr) == mapid) {
        file = spt_get_origin(spt, addr);
        spt_unmap(spt, addr);
        spt_munmap(spt, mapid, addr + PGSIZE);
    }
    return file;
}

/* Removes all the elements from SPT. */
void spt_clear(struct spt *spt) {
    hash_clear(&spt->table, &spt_unmap_elem);
}

/* Evicts the frame mapped to by ADDR. */
void spt_evict(struct spt *spt, void *addr) {
    struct spt_elem *elem = spt_get_entry(spt, addr);
    uint32_t *pd = thread_current()->pagedir;
    void *page = (void *) SPT_KEY(addr);

    /* Write to file if elem is a dirty mmapped file page. */
    if (elem->flags & SPT_MMAP && pagedir_is_dirty(pd, page)) {
        acquire_file_lock();
        struct file *file = elem->origin;
        file_seek(file, elem->file_offset);
        file_write(file, page, elem->file_size);
        release_file_lock();
    }

    /* Write to swap if elem is a dirty non-mmapped file page
       or a non-empty non-file page. */
    else if (elem->flags & SPT_SWAP || pagedir_is_dirty(pd, page)) {
        block_sector_t sector = swap_page_out(pagedir_get_page(pd, page));

        elem->origin = (void *) sector;
        elem->flags |= SPT_SWAP;
        elem->flags &= ~SPT_FILE;
    }

    pagedir_set_dirty(pd, page, false);

    /* Clear the page table entry for elem, freeing its frame. */
    pagedir_clear_page(pd, page);
}

/* Returns true if SPT contains an element corresponding with PAGE,
   returns false otherwise. */
bool spt_contains(struct spt *spt, void *page) {
    return spt_get_entry(spt, page) != NULL;
}

/* Returns true if SPT contains no elements, returns false otherwise. */
bool spt_empty(struct spt *spt) {
    return hash_empty(&spt->table);
}

/* Returns the number of elements currently stored in SPT. */
size_t spt_size(struct spt *spt) {
    return hash_size(&spt->table);
}

/* Returns the origin of the element of SPT corresponding with PAGE.
   Returns NULL if no such element exists. */
void *spt_get_origin(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return elem->origin;
    return NULL;
}

/* Returns the flags of the element of SPT corresponding with PAGE.
   Returns 0 if no such element exists. */
uint8_t spt_get_flags(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return elem->flags;
    return 0;
}

/* Returns the file_offset of the element of SPT corresponding with PAGE.
   Returns 0 if no such element exists. */
off_t spt_get_file_offset(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return elem->file_offset;
    return 0;
}

/* Returns the file_size of the element of SPT corresponding with PAGE.
   Returns 0 if no such element exists. */
size_t spt_get_file_size(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return elem->file_size;
    return 0;
}

/* Returns the mapid of the element of SPT corresponding with PAGE.
   Returns -1 if no such element exists. */
mapid_t spt_get_mapid(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return elem->mapid;
    return -1;
}

/* Returns true if the element of SPT corresponding with PAGE is writable.
   Returns false otherwise. */
bool spt_is_writable(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return (elem->flags & SPT_WRITABLE) != 0;
    return false;
}

/* Returns true if the element of SPT corresponding with PAGE is memory mapped.
   Returns false otherwise. */
bool spt_is_mmapped(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return (elem->flags & SPT_MMAP) != 0;
    return false;
}

/* Returns true if the origin of the element of SPT corresponding with PAGE
   is a file, returns false otherwise. */
bool spt_is_origin_file(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return (elem->flags & SPT_FILE) != 0;
    return false;
}

/* Returns true if the origin of the element of SPT corresponding with PAGE
   is a swap slot, returns false otherwise. */
bool spt_is_origin_swap(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return (elem->flags & SPT_SWAP) != 0;
    return false;
}

/* Returns true if the element of SPT corresponding with PAGE
   has no origin, returns false otherwise. */
bool spt_is_origin_empty(struct spt *spt, void *page) {
    struct spt_elem *elem = spt_get_entry(spt, page);

    if (elem)
        return (elem->flags & SPT_ORIGIN) == 0;
    return false;
}
