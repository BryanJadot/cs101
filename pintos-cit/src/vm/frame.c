/*! \file frame.h
 *
 *
 */

#include "vm/frame.h"

#include <hash.h>

#include "userprog/pagedir.h"
#include "threads/init.h"
#include "threads/malloc.h"

static unsigned frame_table_hash (const struct hash_elem *element, void *aux);
static bool frame_tables_less (const struct hash_elem *a,
                                   const struct hash_elem *b,
                                   void *aux);
static void free_elem (struct hash_elem * elem, void * aux);

void frame_init() {
  frame_table = malloc(sizeof (struct hash));
  frame_list = malloc(sizeof (struct list));

  hash_init (frame_table, frame_table_hash, frame_tables_less, NULL);
  list_init (frame_list);
}

void frame_table_destroy () {
  struct hash * ft = frame_table;
  struct list * fl = frame_list;

  frame_table = NULL;
  frame_list = NULL;

  hash_destroy(ft, free_elem);

  free(ft);
  free(fl);
}

static void free_elem (struct hash_elem * elem, void * aux UNUSED) {
  free(hash_entry(elem, struct frame_elem, h_elem));
}

bool frame_table_insert (void * frame_ptr, void * page_ptr) {
  struct frame_elem * elem = malloc(sizeof (struct frame_elem));

  elem->frame_ptr = frame_ptr;
  elem->page_ptr = page_ptr;
  elem->thread = thread_current();

  if (hash_insert(frame_table, &elem->h_elem) != NULL) {
    free (elem);
    return false;
  }

  list_push_back(frame_list, &elem->l_elem);

  return true;
}

void frame_table_delete (void * frame_ptr) {
  struct frame_elem fe;
  struct frame_elem * frame_elem;
  struct hash_elem * he;

  fe.frame_ptr = frame_ptr;
  he = hash_delete (frame_table, &fe.h_elem);

  if (he == NULL) {
    return;
  }

  frame_elem = hash_entry (he, struct frame_elem, h_elem);
  list_remove(&frame_elem->l_elem);

  free(frame_elem);
}

void * frame_table_find (void * frame_ptr) {
  struct frame_elem fe;
  struct hash_elem * he;

  fe.frame_ptr = frame_ptr;
  he = hash_find (frame_table, &fe.h_elem);

  fe = *hash_entry (he, struct frame_elem, h_elem);
  return he != NULL ? fe.page_ptr : NULL;
}

static unsigned frame_table_hash (const struct hash_elem *element,
                                  void * aux UNUSED) {
  ASSERT (element != NULL);

  struct frame_elem *elem = hash_entry(element, struct frame_elem, h_elem);

  return hash_bytes (&elem->frame_ptr, sizeof (elem->frame_ptr));
}

static bool frame_tables_less (const struct hash_elem *a,
                                   const struct hash_elem *b,
                                   void *aux UNUSED) {
  ASSERT (a != NULL && b != NULL);

  struct frame_elem *frame_a = hash_entry(a, struct frame_elem, h_elem);
  struct frame_elem *frame_b = hash_entry(b, struct frame_elem, h_elem);

  return frame_a->frame_ptr < frame_b->frame_ptr;

}

void * frame_find_evict () {
  struct list_elem *lst_ptr, *next_ptr;
  struct frame_elem *fe;
  void * frame_ptr;
  uint32_t * pagedir;
  
  lst_ptr = list_begin(frame_list);

  while (lst_ptr != NULL) {
    fe = list_entry(lst_ptr, struct frame_elem, l_elem);
    pagedir = fe->thread->pagedir;

    if (pagedir_is_accessed(pagedir, fe->page_ptr)) {
      pagedir_set_accessed(pagedir, fe->page_ptr, false);

      // This could bug out if there's only one frame.
      // Not that that would ever happen.
      next_ptr = list_remove(lst_ptr);
      list_push_back(frame_list, lst_ptr);
      lst_ptr = next_ptr;
    } else {
      frame_ptr = fe->frame_ptr;

      spt_evict(&fe->thread->spt, fe->page_ptr);
      
      return frame_ptr;
    }
  }

  PANIC ("Unable to find frame to evict");
}
