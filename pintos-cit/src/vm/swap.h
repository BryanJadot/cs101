#ifndef VM_SWAP_H
#define VM_SWAP_H

/*
#include <hash.h>
#include <list.h>
*/

#include "devices/block.h"
#include "threads/thread.h"
#include "threads/vaddr.h"

#define SECTORS_PER_PAGE (PGSIZE / BLOCK_SECTOR_SIZE)

/*struct swap_elem {
  void * page_addr;
  struct thread * thread;
  block_sector_t block_index;

  struct list_elem l_elem;
  struct hash_elem h_elem;
}*/

void swap_init(void);
void swap_destroy(void);

block_sector_t swap_page_out(void * frame_addr);
block_sector_t swap_find_free_slot(void);
void swap_page_in(void * frame_addr, block_sector_t swap_ind);

#endif
