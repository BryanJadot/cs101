#ifndef VM_FRAME_H
#define VM_FRAME_H

#include <hash.h>
#include <list.h>

#include "threads/thread.h"

struct frame_elem {
  void * frame_ptr;
  void * page_ptr;
  struct thread *thread;
  struct hash_elem h_elem;
  struct list_elem l_elem;
};

void frame_init (void);
void frame_table_destroy (void);

bool frame_table_insert (void * frame_ptr, void * page_ptr);
void * frame_table_find (void * frame_ptr);
void frame_table_delete (void * frame_ptr);

void * frame_find_evict (void);

#endif
