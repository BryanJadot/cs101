#ifndef VM_PAGE_H
#define VM_PAGE_H

#include <hash.h>
#include "filesys/file.h"

#define SPT_KEY(page) ((uint32_t) page &  0xfffff000)

#define SPT_ORIGIN     0b00000011
#define SPT_FILE       0b00000001
#define SPT_SWAP       0b00000010
#define SPT_WRITABLE   0b00000100
#define SPT_MMAP       0b00001000

struct spt_elem {
    struct hash_elem elem;
    void *page;
    uint8_t flags;
    off_t file_offset;
    size_t file_size;
    void *origin;
    mapid_t mapid;
};

struct spt {
    struct hash table;
};

bool spt_init(struct spt *);
void spt_destroy(struct spt *);

void spt_map_to_file(struct spt *, void *, struct file *, off_t, size_t, bool);
void spt_map_to_swap(struct spt *, void *, void *, bool);
void spt_map_to_empty(struct spt *, void *, bool);
void spt_mmap(struct spt *, void *, mapid_t, struct file *, off_t, size_t, bool);

bool spt_unmap(struct spt *, void *);
struct file *spt_munmap(struct spt *, mapid_t, void *);
void spt_clear(struct spt *);
void spt_evict(struct spt *, void *);

bool spt_contains(struct spt *, void *);
bool spt_empty(struct spt *);
size_t spt_size(struct spt *);

void *spt_get_origin(struct spt *, void *);
uint8_t spt_get_flags(struct spt *, void *);
off_t spt_get_file_offset(struct spt *, void *);
size_t spt_get_file_size(struct spt *, void *);
mapid_t spt_get_mapid(struct spt *, void *);

bool spt_is_writable(struct spt *, void *);
bool spt_is_mmapped(struct spt *, void *);
bool spt_is_origin_file(struct spt *, void *);
bool spt_is_origin_swap(struct spt *, void *);
bool spt_is_origin_empty(struct spt *, void *);

#endif //VM_PAGE_H
