#include "swap.h"

#include <bitmap.h>

#include "devices/block.h"
#include "threads/init.h"
#include "threads/thread.h"
#include "userprog/pagedir.h"

/*static unsigned swap_table_hash (const struct swap_elem *element,
                                 void * aux UNUSED);

static bool swap_tables_less (const struct swap_elem *a,
                              const struct swap_elem *b,
                              void *aux UNUSED);*/

static struct block * get_swap_block(void);

//static void free_elem (struct hash_elem * elem, void * aux UNUSED);

void swap_init(void) {
  block_sector_t size = block_size(get_swap_block()) / SECTORS_PER_PAGE;

  swap_list = bitmap_create(size);
  bitmap_set_all(swap_list, true);
  /*  struct block * swap_block;
  struct swap_elem * swap_elem;
  block_sector_t size;
  uint32_t i;

  ASSERT (size % SEGMENTS_PER_PAGE == 0);

  swap_table = malloc(sizeof (struct hash));
  swap_list = malloc(sizeof (struct list));

  hash_init (swap_table, swap_table_hash, swap_tables_less, NULL);
  list_init (swap_list);

  size = block_size(swap_block);
  for (i = 0; i < size; i += SEGMENTS_PER_PAGE) {
    swap_elem = malloc(sizeof (swap_elem));
    swap_elem->block_index = i;
    swap_elem->page_addr = NULL;
    swap_elem->thread = NULL;

    list_insert(swap_list, swap_elem->l_elem);
    hash_insert(swap_table, swap_elem->h_elem);
    }*/
}

void swap_destroy() {
  bitmap_destroy(swap_list);
  /*struct * hash st = swap_table;
  struct * list sl = swap_list;
  
  struct * list_elem l_elem, temp_elem;

  swap_table = NULL;
  swap_list = NULL;

  for (l_elem = list_begin(sl); l_elem != NULL;) {
    temp_elem = l_elem;
    l_elem = list_next(l_elem);
    free(list_entry(temp_elem));
  }
  free(sl);

  hash_destroy(st, free_elem);
  free(st);*/
}

block_sector_t swap_page_out(void * frame_addr) {
  /*struct swap_elem se;
  uint32_t i;
  struct block * swap_block = get_swap_block();

  se.page_addr = page_addr;
  se.thread = thread;
  se = *hash_entry(hash_find(swap_table, &se.page_addr));

  se.thread = thread;
  se.block_index = swap_ind * SEGMENTS_PER_BLOCK;

  list_remove(&se.l_elem);
  */
  block_sector_t free_slot = swap_find_free_slot();
  struct block * swap_block = get_swap_block();
  uint32_t i;

  for (i = 0; i < SECTORS_PER_PAGE; i++) {
    block_write(swap_block, free_slot + i, frame_addr + i * BLOCK_SECTOR_SIZE);
  }

  bitmap_set(swap_list, free_slot / SECTORS_PER_PAGE, false);
  
  return free_slot;
}

block_sector_t swap_find_free_slot() {
  size_t first = bitmap_scan(swap_list, 0, 1, true) * SECTORS_PER_PAGE;

  if (first == BITMAP_ERROR) {
    PANIC ("Swap space full");
  }

  return first;
}

/*uint32_t swap_find_swap_index(void * page_addr, struct thread * thread) {
  struct swap_elem se;
  uint32_t i;
  struct block * swap_block = get_swap_block();

  se.page_addr = page_addr;
  se.thread = thread;
  return hash_entry(hash_find(swap_table, &se.page_addr))->block_index /
    SEGMENTS_PER_BLOCK;

}*/

void swap_page_in(void * frame_addr, block_sector_t swap_ind) {
  uint32_t i;
  struct block * swap_block = get_swap_block();

  for (i = 0; i < SECTORS_PER_PAGE; i++) {
    block_read(swap_block, swap_ind + i, frame_addr + i * BLOCK_SECTOR_SIZE);
  }

  bitmap_set(swap_list, swap_ind / SECTORS_PER_PAGE, true);
}

static struct block * get_swap_block() {
  return block_get_role(BLOCK_SWAP);
}
/*
static void free_elem (struct hash_elem * elem, void * aux UNUSED) {
  free(hash_entry(elem, struct swap_elem, h_elem);
}

static unsigned swap_table_hash (const struct swap_elem *element,
                                 void * aux UNUSED) {
  ASSERT (element != NULL);

  struct swap_elem *elem = hash_entry(element, struct swap_elem, h_elem);

  return hash_bytes (&elem->page_addr,
                     sizeof (elem->page_addr) + sizeof(elem->thread));
}

static bool swap_tables_less (const struct swap_elem *a,
                              const struct swap_elem *b,
                              void *aux UNUSED) {
  ASSERT (a != NULL && b != NULL);

  struct swap_elem *swap_a = hash_entry(a, struct swap_elem, h_elem);
  struct swap_elem *swap_b = hash_entry(b, struct swap_elem, h_elem);

  if (frame_a->thread != frame_b->thread) {
    return frame_a->thread < frame_b->thread;
  }

  return frame_a->page_addr < frame_b->page_addr;
  }*/
