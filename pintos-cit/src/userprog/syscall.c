#include "userprog/syscall.h"
#include "userprog/process.h"
#include "userprog/gdt.h"
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include "devices/input.h"
#include "devices/shutdown.h"
#include "filesys/directory.h"
#include "filesys/filesys.h"
#include "filesys/file.h"
#include "filesys/inode.h"
#include "lib/user/syscall.h"
#include "threads/interrupt.h"
#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/thread.h"
#include "threads/vaddr.h"

/* Useful for extracting stack arguments in the syscall handler */
#define GET_ARGS(TYPE, INDEX) *(TYPE*)get_arg(f->esp, INDEX)

/* Useful to test that the address is legal */
#define BAD_ADDRESS(ADDR) ((!is_user_vaddr(ADDR)) || \
                           get_user((uint8_t *)ADDR) == -1)
#define CHECK_BYTE(VAR, ADDR) ((!is_user_vaddr(ADDR)) || \
                               (VAR = get_user((uint8_t *)ADDR)) == -1)

static struct lock file_lock;
static void syscall_handler(struct intr_frame *);
static int get_user(const uint8_t *uaddr);
static bool put_user(uint8_t *udst, uint8_t byte);
static int add_to_fdt(void *f);
static void *get_from_fdt(int fd);
static int remove_from_fdt(int fd);
static bool is_dir(int fd);

static void *get_arg(void *esp, int index) {
  esp += index * 4;
  if (BAD_ADDRESS(esp) || BAD_ADDRESS(esp + 4)) {
    exit_handler(-1);
  }

  return esp;
}

static bool bad_string(const char *uaddr) {
    int i;
    char byte = 1;
    for (i = 0; byte != '\0'; i++)
        if CHECK_BYTE(byte, uaddr + i)
            return true;
    return false;
}

void syscall_init(void) {
  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&file_lock);
}

static void syscall_handler(struct intr_frame *f) {
    int call_num = GET_ARGS(int, 0);

    /* Switch on the call number to determine the function */
    switch(call_num) {
        case SYS_HALT:
            halt_handler();
            break;

        case SYS_EXIT:
            exit_handler(GET_ARGS(int, 1));
            break;

        case SYS_EXEC:
            f->eax = exec_handler(GET_ARGS(const char *, 1));
            break;

        case SYS_WAIT:
            f->eax = wait_handler(GET_ARGS(pid_t, 1));
            break;

        case SYS_CREATE:
            f->eax = create_handler(GET_ARGS(const char *, 1), 
                                    GET_ARGS(unsigned, 2));
            break;

        case SYS_REMOVE:
            f->eax = remove_handler(GET_ARGS(const char *, 1));
            break;

        case SYS_OPEN:
            f->eax = open_handler(GET_ARGS(const char *, 1));
            break;

        case SYS_FILESIZE:
            f->eax = filesize_handler(GET_ARGS(int, 1));
            break;

        case SYS_READ:
            f->eax = read_handler(GET_ARGS(int, 1),
                                  GET_ARGS(void *, 2),
                                  GET_ARGS(unsigned, 3));
            break;

        case SYS_WRITE:
            f->eax = write_handler(GET_ARGS(int, 1),
                                   GET_ARGS(const void *, 2),
                                   GET_ARGS(unsigned, 3));
            break;

        case SYS_SEEK:
            seek_handler(GET_ARGS(int, 1), GET_ARGS(unsigned, 2));
            break;

        case SYS_TELL:
            f->eax = tell_handler(GET_ARGS(int, 1));
            break;

        case SYS_CLOSE:
            close_handler(GET_ARGS(int, 1));
            break;

        case SYS_CHDIR:
            f->eax = chdir_handler(GET_ARGS(const char *, 1));
            break;

        case SYS_MKDIR:
            f->eax = mkdir_handler(GET_ARGS(const char *, 1));
            break;

        case SYS_READDIR:
            f->eax = readdir_handler(GET_ARGS(int, 1), GET_ARGS(char *, 2));
            break;

        case SYS_ISDIR:
            f->eax = isdir_handler(GET_ARGS(int, 1));
            break;

        case SYS_INUMBER:
            f->eax = inumber_handler(GET_ARGS(int, 1));
            break;

        default:
            /* In case we have a weird system call, respond appropriately */
            switch (f->cs) {
                case SEL_UCSEG:
                    /* User's code segment, so it's a user exception, as we
                       expected.  Kill the user process.  */
                    printf("User made an invalid system call.");
                    intr_dump_frame(f);
                    thread_exit(); 

                case SEL_KCSEG:
                    /* Kernel's code segment, which indicates a kernel bug.
                       Kernel code shouldn't be making this interrupt. */
                    intr_dump_frame(f);
                    PANIC("Kernel bug - unexpected interrupt in kernel"); 

                default:
                    /* Some other code segment?  Shouldn't happen.  Panic the
                       kernel. */
                    printf("Invalid system call from an unknown segment");
                    thread_exit();
            }
            break;
    }    
}

/* Shutdown */
void halt_handler(void) {
    shutdown_power_off();
}

/* Exit the program properly and exit */
void exit_handler(int status) {
    process_exit_with_status(status);
}

/* Execute the specified file */
pid_t exec_handler(const char *file) {
    if (bad_string(file))
        return -1;

    //lock_acquire(&file_lock);
    int pid = process_execute(file, thread_current()->cwd);
    //lock_release(&file_lock);

    return pid;
}

/* Wait for the specified process */
int wait_handler(pid_t pid) {
    return process_wait(pid);
}

/* Create a file */
bool create_handler(const char *file, unsigned initial_size) {
  if (bad_string(file)) {
    exit_handler(-1);
    return false;
  }

  //lock_acquire(&file_lock);
  bool result = filesys_create(file, initial_size, false);
  //lock_release(&file_lock);
  return result;
}

/* Remove a file from the file system */
bool remove_handler(const char *file) {
  if (bad_string(file)) {
    return false;
  }

  //lock_acquire(&file_lock);
  bool result = filesys_remove(file);
  //lock_release(&file_lock);
  return result;
}

/* Open a file and return its file descriptor */
int open_handler(const char *file) {
  int fd;

  if (bad_string(file)) {
    exit_handler(-1);
    return -1;
  }
  
  //lock_acquire(&file_lock);
  struct file *open_file = filesys_open(file);

  if (open_file == NULL) {
    //lock_release(&file_lock);
    return -1;
  }

  fd = add_to_fdt((struct file *) open_file);
  
  /* File table is full */
  if (fd == -1) {
    open_file->is_dir ? dir_close((struct dir *) open_file) : file_close(open_file);
    
    //lock_release(&file_lock);
    return -1;
  }

  //lock_release(&file_lock);
  
  return fd;
}

/* Returns the size of the file */
int filesize_handler(int fd) {
  struct file *read_file;
  int len = -1;

  //lock_acquire(&file_lock);

  read_file = get_from_fdt(fd);
  if (read_file == NULL || read_file->is_dir) {
    //lock_release(&file_lock);
    return -1;
  }


  len = file_length(read_file);
  //lock_release(&file_lock);

  return len;
}

/* Read from a file or from stdin */
int read_handler(int fd, void *buffer, unsigned size) {
  struct file *read_file;
  unsigned count = 0;
  char *buff;
  int result;

  if (BAD_ADDRESS(buffer) || BAD_ADDRESS(buffer + size)) {
    exit_handler(-1);
    return -1;
  }

  //lock_acquire(&file_lock);

  /* Read from stdin */
  if (fd == STDIN_FILENO) {
    buff = (char *)buffer;

    while (count < size) {
      *buff = (char)input_getc();

      count += sizeof(char);
      buff++;
    }

    //lock_release(&file_lock);
    return size;
  } else {
    /* Read from an open file */
    read_file = get_from_fdt(fd);

    if (read_file == NULL || read_file->is_dir) {
      //lock_release(&file_lock);
      return -1;
    }

    result = file_read(read_file, buffer, size);
    //lock_release(&file_lock);
    return result;
  }
}

/* Write to a file or stdout */
int write_handler(int fd, const void *buffer, unsigned size) {
  struct file *write_file;
  int result;

  if (BAD_ADDRESS(buffer) || BAD_ADDRESS(buffer + size)) {
    exit_handler(-1);
    return -1;
  }

  //lock_acquire(&file_lock);
  /* Write to stdout */
  if (fd == STDOUT_FILENO) {
    putbuf(buffer, size);

    //lock_release(&file_lock);
    return size;
  } else {
    /* Write to an open file */
    write_file = get_from_fdt(fd);
    if (write_file == NULL || write_file->is_dir) {
      //lock_release(&file_lock);
      return -1;
    }

    result = file_write(write_file, buffer, size);
    //lock_release(&file_lock);
    return result;
  }
}

/* Move forward in a file by position */
void seek_handler(int fd, unsigned position) {
  struct file *read_file;

  //lock_acquire(&file_lock);
  read_file = get_from_fdt(fd);
  if (read_file == NULL || read_file->is_dir) {
    //lock_release(&file_lock);
    return;
  }

  file_seek(read_file, position);
  //lock_release(&file_lock);
}

/* Report the current position in a file */
unsigned tell_handler(int fd) {
  //lock_acquire(&file_lock);
  struct file *read_file = get_from_fdt(fd);
  unsigned result;

  
  if (read_file == NULL || read_file->is_dir) {
    //lock_release(&file_lock);
    return 0;
  }

  result = file_tell(read_file);
  //lock_release(&file_lock);
  return result;
}

/* Close a file */
void close_handler(int fd) {
  //lock_acquire(&file_lock);

  void *file = get_from_fdt(fd);

  if (file == NULL) {
    //lock_release(&file_lock);
    return;
  }

  if (((struct file *) file)->is_dir) {
    dir_close((struct dir *) file);
  } else {
    file_close((struct file *) file);
  }
  remove_from_fdt(fd);

  //lock_release(&file_lock);
}

bool chdir_handler(const char *dir) {
  if (bad_string(dir)) {
    exit_handler(-1);
    return false;
  }
  
  //lock_acquire(&file_lock);
  bool success = filesys_change_cwd(dir);
  //lock_release(&file_lock);
  return success;
}

bool mkdir_handler(const char *dir) {
  if (bad_string(dir)) {
    exit_handler(-1);
    return false;
  }

  //lock_acquire(&file_lock);
  bool success = filesys_create(dir, 0, true);
  //lock_release(&file_lock);
  return success;
}

bool readdir_handler(int fd, char name[READDIR_MAX_LEN + 1]) {
  if (BAD_ADDRESS(name) || BAD_ADDRESS(name + READDIR_MAX_LEN + 1)) {
    exit_handler(-1);
    return false;
  }

  if (!is_dir(fd))
    return false;

  struct dir *dir = get_from_fdt(fd);

  bool success;
  //lock_acquire(&file_lock);
  while ((success = dir_readdir(dir, name)) &&
         (!strcmp(name, ".") || !strcmp(name, ".."))) {}
  //lock_release(&file_lock);
  return success;
}

bool isdir_handler(int fd) {
    return is_dir(fd);
}

int inumber_handler(int fd) {
    return inode_get_inumber(dir_get_inode((struct dir *) get_from_fdt(fd)));
}

/* Get a file corresponding to a file descriptor */
void *get_from_fdt(int fd) {
  fd = fd - 2;

  if (fd < 0 || fd >= MAX_OPEN_FILES) {
    return NULL;
  }

  return thread_current()->fdt[fd];
}

/* Add a file to the descriptor table */
static int add_to_fdt(void *d) {
  int i;
  struct thread *t = thread_current();

  if (d == NULL) {
    return -1;
  }

  for (i = 0; i < MAX_OPEN_FILES; i++) {
    if (t->fdt[i] == NULL) {
      t->fdt[i] = d;
      return i + 2; /* Plus two to skip stdin and stdout */
    }
  }

  return -1;
}

static bool is_dir(int fd) {
  return ((struct file *) thread_current()->fdt[fd - 2])->is_dir;
}

/* Remove the file from the file descriptor table */
static int remove_from_fdt(int fd) {
  fd = fd - 2;

  if (fd >= 0 && fd < MAX_OPEN_FILES) {
    thread_current()->fdt[fd] = NULL;
    return 0;
  } else {
    return -1;
  }
}

/*! Reads a byte at user virtual address UADDR.
    UADDR must be below PHYS_BASE.
    Returns the byte value if successful, -1 if a segfault occurred. */
static int get_user(const uint8_t *uaddr) {
    int result;
    asm ("movl $1f, %0; movzbl %1, %0; 1:"
	 : "=&a" (result) : "m" (*uaddr));
    return result;
}

/*! Writes BYTE to user address UDST.
    UDST must be below PHYS_BASE.
    Returns true if successful, false if a segfault occurred. */
static bool put_user(uint8_t *udst, uint8_t byte) {
    int error_code;
    asm ("movl $1f, %0; movb %b2, %1; 1:"
	 : "=&a" (error_code), "=m" (*udst) : "q" (byte));
    return error_code != -1;
}
