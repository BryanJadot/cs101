#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include "lib/user/syscall.h"

struct fdt_entry {
  bool is_dir;
  union {
    struct dir * dir;
    struct file * file;
  };
};

void syscall_init(void);

void halt_handler(void);
void exit_handler(int status);
pid_t exec_handler(const char *file);
int wait_handler(pid_t pid);
bool create_handler(const char *file, unsigned initial_size);
bool remove_handler(const char *file);
int open_handler(const char *file);
int filesize_handler(int fd);
int read_handler(int fd, void *buffer, unsigned size);
int write_handler(int fd, const void *buffer, unsigned size);
void seek_handler(int fd, unsigned position);
unsigned tell_handler(int fd);
void close_handler(int fd);
bool chdir_handler(const char *dir);
bool mkdir_handler(const char *dir);
bool readdir_handler(int fd, char name[READDIR_MAX_LEN + 1]);
bool isdir_handler(int fd);
int inumber_handler(int fd);

#endif /* userprog/syscall.h */

