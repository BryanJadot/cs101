#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "lib/user/syscall.h"
#include "threads/thread.h"
#include "threads/synch.h"

/* Data structure used to keep track of and communicate with
   a user process' children. */
struct up_signal {
    pid_t pid;
    bool load;                    /* Did the process successfully load its program? */
    bool done;                    /* Has the process terminated? */
    bool abandoned;               /* Set when the parent process terminates or
                                     waits for this process. */
    int exit_status;              /* If process PID has terminated, the exit status. */
    struct list children;
    struct list_elem child_elem;  /* List element used by parent process. */
    struct semaphore sema;        /* Used by parent to wait for this process. */
};

void init_userprog(struct thread *);
tid_t process_execute(const char *file_name, struct dir* starting_dir);
int process_wait(tid_t);
void process_exit_with_status(int);
void process_exit(void);
void process_activate(void);

#endif /* userprog/process.h */

