%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "shellParser.tab.h"

%}
%option noyywrap

%%

[ \t\r\n]+                        /* eat whitespace */

[^\" \t\r\n\|\<\>]+               { 
                                    yylval.arr1 = strdup(yytext);
                                    return ARG;
                                  }
\"[^\"]*\"                              {
                                    yytext[strlen(yytext) - 1] = '\0';
                                    yylval.arr1 = strdup(yytext + 1);
                                    return ARG;
                                  }

\|                                { return PIPE; }
\<                                { return RD_IN; }
\>                                { return RD_OUT; }

.                                 { printf("Unexpected character: %s", yytext);
                                    yyterminate();
                                  }
%%

void setInput(FILE *input) {
  yyin = input;
}
