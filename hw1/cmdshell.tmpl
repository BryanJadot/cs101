			+------------------+
			|     CS 101OS     |
			| PROJECT 1: SHELL |
			|  DESIGN DOCUMENT |
			+------------------+
				   
---- GROUP ----

>> Fill in the names and email addresses of your group members.

Bryan Jadot <bjadot@caltech.edu>
Eric Gomez <egomez@caltech.edu>

>> Specify how many late tokens you are using on this assignment: 1

>> What is the Git repository and commit hash for your submission?

   Repository URL: https://BryanJadot@bitbucket.org/BryanJadot/cs101.git
   commit 2aca6bb

---- PRELIMINARIES ----

We used flex/bison to parse the user's input and convert it into a data
structure that is used to execute the command.  So you will need flex and bison
to compile this.

			   COMMAND SHELL
			   =============

---- DATA STRUCTURES ----
A1:
Let us consider "a arg1 arg2 | b arg1 | c arg1 arg2 arg3"

The data structure that we are trying to build is as follows:
We want a 2d array of strings (2d array of chars).  Each slot in the array is
a pointer to the following array:

The first slot in the array is a pointer to the number 1, indicating that we
are considering a normal command and not a redirect.  The second slot in the
array holds a pointer to the length of the array.  The rest of the array are
the commands.  The array is null terminated.  The array for this command is

0 -> [1, 3, "a", "arg1", "arg2", NULL]
1 -> [1, 2, "b", "arg1", NULL]
2 -> [1, 4, "c", "arg1", "arg2", "arg3", NULL]
3 -> [NULL]
4 -> [NULL]

The last two entries in this array hold pointers to the file paths for initial
input redirection and final output redirection, or NULL if no redirection
is necessary.

This will then be passed to the portion of the code responsible for execution.

This parsing happens with the help of flex and bison.

Our lexer initially labels all the a, arg1, arg2, b, c, arg3 as arguments and
the pipes as pipes.

Then adjacent arguements are combined into a command.  In doing so, an
individual array is created (e.g. [1, 3, "a", "arg1", "arg2", NULL])

A command group is transformed into a commands.  This is where the large
array is created.  Commands will eat up any adjacent command to form a
larger commands.  Finally this is stored in the inputArray variable.

This inputArray is copied into our execution part, where each action is
executed and then the result is piped into the next child process.

A2
Consider "a arg1 arg2 < inp.txt | b arg1 | c arg1 arg2 arg3 > out.txt"

We want to build the following 3d array:
0 -> [1, 3, "a", "arg1", "arg2", NULL]
1 -> [1, 2, "b", "arg1", NULL]
2 -> [1, 4, "c", "arg1", "arg2", "arg3", NULL]
3 -> ["inp.txt"]
4 -> ["out.txt"]

The last two commands hold the redirections.  We always put the input
redirection as the second to last command and the output redirection
as the last command.

The method for parsing is identical to A1, except when we parse commands
RD_OUT (or RD_IN) argument, we write the argument into the appropriate
redirect section of the data structure.

---- ALGORITHMS ----

A3:
This is very easy.  We build the data structure in the manner described in A2.
In the last two slots we have the file redirects.  We pass the file names to
the executeAll function, so that the child processes executing the first and
last programs can redirect stdin and stdout as neccessary upon creation.

A4:
At the top of our executeAll function, we create all of our pipes.  Then after
we fork, each child process replaces stdin and stdout with the appropriate
pipe file descriptors and closes all unused pipe file descriptors. The parent
thread then closes all the pipes file descriptors before waiting on the
children.

Now, when the processes print to stdout and read from stdin, they will actually
be interacting with the pipes.

A5:
After parsing input, a single child is forked off to call executeAll.  This
child then forks off the processes with perform the commands, and is
responsible for reaping them.  The shell process is responsible for reaping
this single child process.

A6:
The child process which calls executeAll() stores the input file path and
output file path, and creates all the pipes; then, the processes which are
forked off from that process to execute a single command are each responsible
redirecting their own input and output before calling execute().

---- RATIONALE ----

A7
The chdir and exit commands both involve changing the state of the shell
process (in one case changing the working directory and in the other case,
terminating).  Since the operating system prohibits processes from manipulating
the state of other processes for security reasons, an external process cannot
do this.

A8
We chose this design for its simplicity and extensibility.

Parsing the input all at once makes it easy to add new features since
new syntax does not interrupt the flow of the program.

Storing the commands this way makes executing them as easy as passing pointers
to parts of the array to execvp().  Also, more metadata can be added to the end
of the array if necessary without altering the execution code.

			  SURVEY QUESTIONS
			  ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the feedback survey on the course
website.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?

