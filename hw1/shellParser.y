/* Parses a shell command */
%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int yyerror(const char *s);
int yylex();
void setInput();
void print_input_arr(char ***input, int length);

static char*** inputArr;
static int size;

%}

%union
{
  char*** arr3;
  char** arr2;
  char* arr1;
}

%defines

%token <arr1> ARG

%token PIPE
%token RD_IN
%token RD_OUT

%type <arr1> argument;

%type <arr2> command;

%type <arr3> commands;

%%

/* The input consists of an alternating sequence of pipes and commands,
   possibly with input redirection in the first command and output redirection
   in the last command. */
input : commands
        { inputArr = $1; }
;

commands : command
    {
      $$ = malloc(3 * sizeof(char**));

      $$[0] = $1;
      $$[1] = malloc(sizeof(char*));
      $$[2] = malloc(sizeof(char*));

      /* NULL-terminate the command */
      $$[0][*$1[1] + 2] = NULL;
      
      $$[1][0] = NULL;
      $$[2][0] = NULL;
          
      size = 3;
    }
  | commands RD_IN argument
    {
      if ($$[size - 2][0] != NULL)
        free($$[size - 2][0]);
      $$[size - 2][0] = $3;
    }
  | commands PIPE command
    {
      size++;
      $$ = realloc($$, (size + 1) * sizeof(char**));
      
      $$[size - 1] = $$[size - 2];
      $$[size - 2] = $$[size - 3];
      $$[size - 3] = $3;
      
      /* NULL-terminate the new command */
      $$[size - 3][*$3[1] + 2] = NULL;
    }
  | commands RD_OUT argument
    {
      if ($$[size - 1][0] != NULL)
        free($$[size - 1][0]);
      $$[size - 1][0] = $3;
    }
        
command : argument
    {
      $$ = malloc(4 * sizeof(char*));
          
      $$[0] = malloc(sizeof(char));
      *$$[0] = 1;
      $$[1] = malloc(sizeof(char));
      *$$[1] = 1;

      $$[2] = $1;
    }
  | command argument
    {
      $$ = realloc($$, (*$$[1] + 3) * sizeof(char*));
      (*$$[1])++;
      $$[1 + *$$[1]] = $2;
    }
        
argument : ARG
    { 
      $$ = $1;
    }
;

%%

int yyerror(const char * s)
{
  fprintf(stderr, "Parse error: %s", s);
  return 0;
}

/* Parse the input */
void parse(FILE *input)
{
  setInput(input);
  yyparse();
}

/* Return the size of the current output array.
   Should only be called after a call to parse(). */
int getSize() {
  return size;
}

/* Copy the current output array to the passed location.
   frees the current output array.
   Should only be called once after each call to parse(). */
void getOutput(char*** array) {
  memcpy(array, inputArr, size * sizeof(char**));
  free(inputArr);
}