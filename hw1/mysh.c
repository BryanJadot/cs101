#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define INPUT_SIZE 1024
#define IN_REDIRECT '<'
#define PIPE '|'
#define OUT_REDIRECT '>'

/* Functions to interact with parser. */
extern void parse(FILE *input);
extern int getSize();
extern void getOutput(char*** array);

typedef char** cmd_t;  /* type of parsed command structure */

/*
This function executes a single external command pointed to by cmd,
terminating the calling process.
*/
int execute(cmd_t cmd) {
  /* execute the command */
  execvp(cmd[2], &cmd[2]);
  /* no return expected after execlp */
  perror("execlp");
  _exit(1);
  
  /* should never reach here */
  return -1;
}

/*
This function executes a piped sequence of external commands of length cmdc
pointed to by the array cmdv, then terminates the calling process.
Initial input is redirected from inpath and final output to outpath
if they are not null.
*/
int executeAll(int cmdc, cmd_t cmdv[], char* inpath, char* outpath) {
  int fd, i, child, p;
  int npipes = cmdc - 1;
  int** pipes;

  /* set up the necessary pipes */
  if (npipes > 0) {
    pipes = malloc(npipes * sizeof(int*));
    for (i = 0; i < npipes; i++) {
      pipes[i] = malloc(2 * sizeof(int));
      if (pipe(pipes[i]) < 0) {
        perror("pipe error");
        _exit(1);
      }
    }
  }

  /* fork off children to execute commands */
  for (child = 0; child < cmdc; child++) {
    switch (fork()) {
      case -1:
        perror("fork");
        _exit(1);
      
      case 0: /* child process */
        /* close unnecessary pipe ends and redirect i/o through pipes */
        for (p = 0; p < npipes; p++) {
          switch (p - child) {
            case -1: /* will read data from this pipe */
              if (close(pipes[p][1]) < 0) {
                perror("close");
                _exit(1);
              } else if (dup2(pipes[p][0], STDIN_FILENO) < 0) {
                perror("dup2");
                _exit(1);
              }
              break;
            case 0: /* will write data to this pipe */
              if (close(pipes[p][0]) < 0) {
                perror("close");
                _exit(1);
              } else if (dup2(pipes[p][1], STDOUT_FILENO) < 0) {
                perror("dup2");
                _exit(1);
              }
              break;
            default: /* will not use this pipe at all */
              if (close(pipes[p][0]) < 0) {
                perror("close");
                _exit(1);
              }
              if (close(pipes[p][1]) < 0) {
                perror("close");
                _exit(1);
              }
              break;
          }
        }
        
        /* redirect stdin if necessary */
        if (child == 0 && inpath != NULL) {
          if ((fd = open(inpath, O_RDONLY)) < 0) {
            perror("input redirection: open");
            exit(1);
          }
          else if (dup2(fd, STDIN_FILENO) < 0) {
            perror("input redirection: dup2");
            exit(1);
          }
        }
        
        /* redirect stdout if necessary */
        if (child == cmdc - 1 && outpath != NULL) {
          if ((fd = open(outpath, O_WRONLY | O_CREAT | O_TRUNC, 0777)) < 0) {
            perror("output redirection: open");
            exit(1);
          }
          else if (dup2(fd, STDOUT_FILENO) < 0) {
            perror("output redirection: dup2");
            exit(1);
          }
        }
                
        /* execute a command */
        execute(cmdv[child]); /* should not return */
        perror("execute");
        _exit(1);
      
      default: /* parent process */
        /* fork off next child */
        break;
    }
  }
  
  /* close all the pipes so the children can use them properly */
  for (p = 0; p < npipes; p++) {
    if (close(pipes[p][0]) < 0) {
      perror("close");
      _exit(1);
    }
    if (close(pipes[p][1]) < 0) {
      perror("close");
      _exit(1);
    }
  }
  
  /* wait for all the children to finish */
  for (child = 0; child < cmdc; child++) {
    if (wait(NULL) < 0) {
      perror("wait");
      _exit(1);
    }
  }
  
  /* free dynamic memory */
  for (i = 0; i < npipes; i++) 
    free(pipes[i]);
  if (npipes > 0)
    free(pipes);
  
  /* signal completion to calling thread */
  _exit(0);
  
  /* should never reach here */
  return -1;
}


/*
This function detects and executes built-in commands and spawns
a child process to execute external commands.
*/
void handleInput(cmd_t *input, int length) {
  /* check for input/output redirection */
  char* in = input[length - 2][0];
  char* out = input[length - 1][0];
  
  /* check for built-in commands */
  char* path;
  if (length == 3 && in == NULL && out == NULL) {
    if (strcmp(input[0][2], "cd") == 0 || strcmp(input[0][2], "chdir") == 0) {
      /* change directory */
      if (*input[0][1] == 1)
        /* no argument; switch to HOME */
        path = getenv("HOME");
      else
        path = input[0][3];
        
      if (chdir(path) < 0) {
        perror("chdir");
      }
      return;
    }
    if (strcmp(input[0][2], "exit") == 0) {
      /* exit */
      exit(0);
    }
  }
  
  /* execute the external commands */
  switch (fork()) {
    case -1:
      perror("fork");
      break;
    case 0: /* child; execute commands */
      executeAll(length - 2, input, in, out);
      /* should never reach here */
      perror("executeAll");
      _exit(1);
    default: /* parent thread */
      if (wait(NULL) < 0)
        printf("failed to perform command");
      break;
  }
  
  return;
}

/* Print the input array */
void print_input_arr(char ***input, int length) {
  int i, j;

  for (i = 0; i < length - 2; i++) {
    fprintf(stderr, "Array %i\n", i);
    fprintf(stderr, "%i\n", *input[i][0]);
    if (*input[i][0] == 1) {
      fprintf(stderr, "%i\n", *input[i][1]);
      for (j = 2; j < *input[i][1] + 3; j++) {
        fprintf(stderr, "|%s|\n", input[i][j]);
      }
    } else {
      fprintf(stderr, "|%s|\n", input[i][1]);
    }
    fprintf(stderr, "\n");
  }
  
  fprintf(stderr, "Array %i\n", length - 2);
  fprintf(stderr, "|%s|\n", input[length - 2][0]);
  fprintf(stderr, "\n");
  
  fprintf(stderr, "Array %i\n", length - 1);
  fprintf(stderr, "|%s|\n", input[length - 1][0]);
  fprintf(stderr, "\n");
}

/* Helper method that frees the inputArr variable */
void free_input_arr(char ***input, int length) {
  int i, j;

  /* free command data */
  for (i = 0; i < length - 2; i++) {
    if (*input[i][0] == 1) {
      for (j = 2; j < *input[i][1] + 2; j++) {
        free(input[i][j]);
      }
    }
    
    free(input[i][0]);
    free(input[i][1]);

    free(input[i]);
  }
  
  /* free redirection data */
  for (i = length - 2; i < length; i++) {
    if (input[i][0] != NULL)
      free(input[i][0]);
    
    free(input[i]);
  }

  free(input);
}

void parse_input(char* input) {
  FILE *in = fmemopen (input, strlen(input), "r");
  
  int size;
  char*** inArray;

  parse(in);
  
  size = getSize();
  inArray = malloc(size * sizeof(char**));
  getOutput(inArray);
  
  handleInput(inArray, size);
  
  free_input_arr(inArray, size);
}

/* Start the program and display the shell */
int main(int argc, char *argv[]) {
  char *cwd;
  char input[INPUT_SIZE];

  while (1) {
    cwd = getcwd(NULL, 0);
    printf("%s:%s> ", getlogin(), cwd);
    fgets(input, INPUT_SIZE, stdin);

    parse_input(input);

    free(cwd);
  }
}
